#!/bin/bash

# ----------------------------------------------------------------
# This script calls on QUAST to assess the quality of multiple
# assemblies using an index file

# Note
#     this requires a TSV index file of the absolute file paths
#     to the assemblies (fasta files) to be assessed and a label
#     for each assembly as:
#
#              label    path/to/assembly.fasta
#               ...             ...

# Command line arguments

# $1 - Index file (tsv) (cannot be empty)
# $2 - Output directory (cannot be empty)
# $3 - Genomesize file (cannot be empty)
# $4 - Reference genome (fasta) (cannot be empty)
# $5 - Reference annotation (gff) (cannot be empty)
# $6 - The number of threads to use (default 3)
# ----------------------------------------------------------------

#SBATCH --job-name=quast
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=5
#SBATCH --mem=20G
#SBATCH --time=10:00:00
#SBATCH --output=quast.log
#SBATCH --partition=pcourseassembly

parent="/data/users/$USER/genome_assembly" # Path to the parent directory
pipeline="$parent/pipelines" # Path to the pipeline directory

cd "$parent" 

module add UHTS/Quality_control/quast/4.6.0;

# get the command line arguments
index=$1
out_dir=$2
genomesize=$3
reference=$4
annotation=$5

threads=$6
if [ -z "$threads" ]; then
    threads=3
fi

# read the indexfile
labels=$( cut -f 1 $index | tr "\n" " " )
assemblies=$( cut -f 2 $index | tr "\n" " " )