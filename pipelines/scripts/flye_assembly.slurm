#!/bin/bash

# ----------------------------------------------------------------
# This script calls on flye to perform genome assembly from raw PacBio reads.
# This script is intended to be used as part of the Snakemake pipeline 
# rather than a stand-alone.

# Command line arguments

# $1 - Input data file (fwd) (cannot be empty)
# $2 - Input data file (rev) (cannot be empty)
# $3 - The number of polishing iterations (default is 5)
# $4 - The output directory
# $5 - The file containing the genome size

# ----------------------------------------------------------------

#SBATCH --job-name=flye
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=80G
#SBATCH --time=24:00:00
#SBATCH --output=flye.log
#SBATCH --partition=pall

# load flye
module load UHTS/Assembler/flye/2.8.3;

# get the command line arguments
infile_fwd=$1
infile_rev=$2

assembly_dir=$4

# get the genome_size
genomesize="$( tail -n 1 $5 )"

if [[ ! -d $assembly_dir ]]; then
    mkdir -p $assembly_dir
fi

polish_iterations=$3
if [[ $polish_iterations == "" ]]; then polish_iterations=5; fi

# call on flye
flye --out-dir $assembly_dir \
    --threads 16 \
    --genome-size $genomesize \
    --iterations $polish_iterations \
    --pacbio-raw $infile_fwd $infile_rev