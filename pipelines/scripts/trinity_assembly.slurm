#!/bin/bash

# ----------------------------------------------------------------
# This script calls on trinity to perform transcriptome assembly from raw PacBio reads.
# This script is intended to be used as part of the Snakemake pipeline 
# rather than a stand-alone.

# Command line arguments

# $1 - Input data file (fwd) (cannot be empty)
# $2 - Input data file (rev) (cannot be empty)
# $3 - The output directory

# ----------------------------------------------------------------

#SBATCH --job-name=trinity
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10
#SBATCH --mem=50G
#SBATCH --time=24:00:00
#SBATCH --output=trinity.log
#SBATCH --partition=pall

# load trinity
module load UHTS/Assembler/trinityrnaseq/2.5.1;


# get the command line arguments
infile_fwd=$1
infile_rev=$2

assembly_dir=$3


if [[ ! -d $assembly_dir ]]; then
    mkdir -p $assembly_dir
fi

# call on trinity
Trinity --seqType fq \
        --CPU 10 --max_memory 50G  \
        --trimmomatic \
        --output $assembly_dir \
        --left $infile_fwd --right $infile_rev 
