#!/bin/bash

# ----------------------------------------------------------------
# This script calls on canu to perform genome assembly from raw PacBio reads.
# This script is intended to be used as part of the Snakemake pipeline 
# rather than a stand-alone.

# Note
#     This assumes RAW reads as input!

# Command line arguments

# $1 - Input data file (fwd) (cannot be empty)
# $2 - Input data file (rev) (cannot be empty)
# $3 - The data source (prefix)
# $4 - The output directory
# $5 - The file containing the genome size
# ----------------------------------------------------------------

#SBATCH --job-name=canu
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem=8G
#SBATCH --time=00:30:00
#SBATCH --output=canu.log
#SBATCH --partition=pall

# load canu and specify the running params
module load UHTS/Assembler/canu/2.1.1;

# get the command line arguments
infile_fwd=$1
infile_rev=$2

prefix=$3
assembly_dir=$4

# get the genome_size
genomesize="$( tail -n 1 $5 )"

if [[ ! -d $assembly_dir ]]; then
    mkdir -p $assembly_dir
fi

# call on flye
canu -p $prefix -d $assembly_dir \
    genomeSize=$genomesize \
    -raw \
    gridEngineResourceOption="--cpus-per-task=THREADS --mem-per-cpu=MEMORY" \
    gridOptions="--partition=pall" \
    -pacbio $infile_fwd $infile_rev \