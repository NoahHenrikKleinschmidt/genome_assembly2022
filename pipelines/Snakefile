
import os, glob

# get the base directory
base = "/data/users/noahkleinschmidt/genome_assembly"

datafiles = [ i.replace(".fastq.gz", "") for i in os.listdir( f"{base}/data" ) if ".fastq.gz" in i ]
sources = set( [ i.split("__")[0] for i in datafiles] )
directions = ( "R1", "R2" )

source_shorts = [ "Illumina" ]
assemblers = [ "canu", "flye", "trinity" ]

rule FastQC:
    input:
        expand( "../data/{file}.fastq.gz", file=datafiles )
    output:
        expand( "../qc/{file}_fastqc.html", file=datafiles )

    threads:
        10

    shell:
        """
        if [[ ! -d ../qc ]]; then mkdir ../qc; fi; 
        module load UHTS/Quality_control/fastqc/0.11.9;
        fastqc {input} -t {threads} -o ../qc
        """

rule MultiQC:
    input:
        expand( "../qc/{file}_fastqc.html", file=datafiles )
    output:
        "../qc/multiqc_report.html"

    conda:
        "../envs/multiqc.yaml"

    shell:
        "multiqc ../qc -o ../qc"


rule JellyFish:
    input:
        expand( "../data/{{source}}__{direction}.fastq.gz", direction=directions )
    output:
        "../qc/kmers/{source}.histo"
    params:
        kmer_size = 19,

    threads:
        10

    run:
        infiles = [ f"<(zcat {i})" for i in input ]
        jfout = str(output).replace(".histo", ".jf")

        shell( 
        """
        if [[ ! -d ../qc/kmers ]]; then mkdir -p ../qc/kmers; fi; 

        module load UHTS/Analysis/jellyfish/2.3.0;

        jellyfish count -m {params.kmer_size} -s 500M -t {threads} -C -o {jfout} {infiles};

        jellyfish histo {jfout} > {output}.histo;

        """ )

rule mine_read_lengths:
    input:
        "../qc/multiqc_report.html"
    output:
        "../qc/read_lengths.txt"

    # conda:
    #     "../envs/basic.yaml"

    run:
        import pandas as pd
        
        src = "../qc/multiqc_data/multiqc_general_stats.txt"
        df = pd.read_csv( src, sep="\t" )

        df["source"] = df["Sample"].apply( lambda x: x.split("__")[0] ) 

        df = df.groupby( "source" ).mean().reset_index()
        df = df.rename( columns = {"FastQC_mqc-generalstats-fastqc-avg_sequence_length" : "length"} )
        
        df = df[ ["source", "length"] ]
        df["length"] = df["length"].astype(int)
        
        df.to_csv( output, sep="\t", index=False, header=False )


rule GenomeScope:
    input: 
        histo = "../qc/kmers/{source}.histo",
        lengths = "../qc/read_lengths.txt"
    output:
        directory ( "../qc/kmers/{source}" )
    params:
        kmer_size = 19,

    conda:
        "../envs/genomescope.yaml"
    
    shell:
        """
        # get genomescope
        if [[ ! -d ../genomescope ]]; then 
            git clone https://github.com/schatzlab/genomescope ../genomescope; 
            records comment ../genomescope -c "the local version of GenomeScope1.0" -f genomescope analysis;
        fi;

        # make the output directory if necessary
        if [[ ! -d {output} ]]; then mkdir {output}; fi;

        # get the read length
        read_length=$( grep {wildcards.source} {input.lengths} | cut -f 2 );

        # run genomescope
        Rscript ../genomescope/genomescope.R {input.histo} {params.kmer_size} $read_length {output} 
        """

rule flye_assemble:
    input:
        datafiles=expand( "../data/{{source}}__{direction}.fastq.gz", direction=directions ),
        sizefile="../assembly/{source}/genomesize.txt"

    output:
        directory ( "../assembly/{source}/flye" )
    params:
        polish_iterations = 5,
    shell:
        """bash ./scripts/flye_assembly.slurm {input.datafiles} {output} {params.polish_iterations} {input.sizefile}"""

rule canu_assemble:
    input:
        datafiles=expand( "../data/{{source}}__{direction}.fastq.gz", direction=directions ),
        sizefile="../assembly/genomesize.txt"
    output:
        directory( "../assembly/{source}/canu" )
    shell:
        """bash ./scripts/canu_assembly.slurm {input.datafiles} {wildcards.source} {output} {input.sizefile}"""

rule trinity_assemble:
    input:
        datafiles=expand( "../data/{{source}}__{direction}.fastq.gz", direction=directions ),
    output:
        directory( "../assembly/{source}/trinity" )
    log:
        "../assembly/{source}/trinity.log"
    shell:
        """bash ./scripts/trinity_assembly.slurm {input.datafiles} {output}"""

rule mine_genomesize:
    input:
        file = "../qc/kmers/{source}/summary.txt"
    output:
        "../assembly/{source}/genomesize.txt"

    params:
        common_genomesize = "../assembly/genomesize.txt",
        outdir = "../assembly/{source}"

    run:
        import re, os
        with open( input.file, "r" ) as f:
            contents = f.readlines()
        
        contents = [ i for i in contents if "Genome Haploid Length" in i ][0]

        genome_size = re.search( r"Genome Haploid Length +([0-9,]+) bp +([0-9,]+) bp", contents )
        minsize = int( genome_size.group(1).replace(",", "") )
        maxsize = int( genome_size.group(2).replace(",", "") )
        size = round( (minsize + maxsize) / 2 )
        raw_size = size

        # format the genomesize from raw float to 5.4M notation
        suffix = {
            3 : "k",
            6 : "M",
            9 : "G",
        }
        tags = list( suffix.keys() )
        idx = 0
        for i in tags:
            if size / 10 ** tags[idx+1] > 1 :
                idx += 1
            
        size = round( size / 10**tags[idx] , 2 )
        size = str(size) + suffix[tags[idx]]


        os.system( f"mkdir -p {params.outdir}" ) 
        os.system( f"""echo "{raw_size}\n{size}" > {output}""" )
        os.system( f"""echo "# source {wildcards.source}\n{raw_size}\n{size}" > {params.common_genomesize}""" )

rule get_fasta_for_assembly:
    output:
        "../assembly/{source}/{assembler}/assembly.fasta"
    run: 
        import os
        files = {
            "flye" : "assembly.fasta",
            "canu" : f"{wildcards.source}.contigs.fasta",
            "trinity" : "Trinity.fasta",
        }
        fastafile = files.get( wildcards.assembler, "No_assembler_found" )
        indir = os.path.dirname( str(output) )
        os.system( f"ln {indir}/{fastafile} {output}" )

rule bowtie_index:
    input:
       "../assembly/{source}/{assembler}/assembly.fasta"
    output:
        directory( "../polishing/{source}/{assembler}/bowtie_index" )

    shell:
        """
        module add UHTS/Aligner/bowtie2/2.3.4.1;

        if [[ ! -d {output} ]]; then mkdir -p {output}; fi;

        bowtie2-build -f {input} {output}/index;
        """

rule map_short_reads:
    input:
        reads = expand( "../data/{{source_short}}__{direction}.fastq.gz", direction=directions ),
        index = "../polishing/{source_data}/{source_index}/bowtie_index"
    output:
        "../polishing/{source_data}/{source_index}/{source_short}/alignment.sam"

    threads: 8

    shell:
        """
        module add UHTS/Aligner/bowtie2/2.3.4.1;

        bowtie2 --sensitive-local -x {input.index}/index -1 {input.reads[0]} -2 {input.reads[1]} -S {output} -p {threads};
        """

rule sam_to_bam:
    input:
        "../polishing/{source_data}/{source_index}/{source_short}/alignment.sam"
    output:
        "../polishing/{source_data}/{source_index}/{source_short}/alignment.bam"
    shell:
        """
        module load UHTS/Analysis/samtools/1.10;

        samtools view -bS {input} > {output};
        """

rule index_bam:
    input:
        "../polishing/{source_data}/{source_index}/{source_short}/alignment_sorted.bam"
    output:
        "../polishing/{source_data}/{source_index}/{source_short}/alignment_sorted.bam.bai"
    shell:
        """
        module load UHTS/Analysis/samtools/1.10;

        samtools index {input};
        """

rule pilon:
    input:
        assembly = "../assembly/{source_data}/{assembler}/assembly.fasta",
        bam = "../polishing/{source_data}/{assembler}/{source_short}/alignment_sorted.bam",
        bam_idx = "../polishing/{source_data}/{assembler}/{source_short}/alignment_sorted.bam.bai"
    output:
        directory( "../polishing/{source_data}/{assembler}/pilon_{source_short}" )
    params:
        prefix = "alignment"
    shell:
        """
        module load UHTS/Analysis/pilon/1.22;
        pilon --genome {input.assembly} --bam {input.bam} --output {params.prefix} --outdir {output};
        """

rule move_pilon:
    input:
        "../polishing/{source_data}/{assembler}/pilon_{source_short}/alignment.fasta"
    output:
        "../polishing/{source_data}/{assembler}/pilon_{source_short}.fasta"
    shell:
        """
        mv {input} {output};
        rm -r ../polishing/{wildcards.source_data}/{wildcards.assembler}/pilon_{wildcards.source_short};
        """

rule sort_alignment:
    input:
        "../polishing/{source_data}/{assembler}/{source_short}/alignment.bam"
    output:
        "../polishing/{source_data}/{assembler}/{source_short}/alignment_sorted.bam"
    threads: 8
    resources:
        tmp_dir = "../polishing/{source_data}/{assembler}/{source_short}/"
    shell:
        """
        module load UHTS/Analysis/samtools/1.10;

        samtools sort -o {output} -O BAM --threads {threads} -T {wildcards.source_short} {input};
        """

rule merqury:
    conda:
        "../envs/merqury.yaml"
    input:
        fasta_unpolished = "../assembly/{source_data}/{assembler}/assembly.fasta",
        fasta_polished = "../polishing/{source_data}/{assembler}/pilon_{source_short}.fasta",
        meryl = "../polishing/all_kmers__{source_short}.meryl"
    output:
        directory( "../summary/merqury/{source_data}/{assembler}/{source_short}" )
    
    resources:
        cores = 10,
        mem = "50G"

    shell:
        """
        mkdir -p {output};

        meryl=$( readlink -f {input.meryl} );
        unpolished=$( readlink -f {input.fasta_unpolished} );
        polished=$( readlink -f {input.fasta_polished} );

        cd {output};

        # run unpolished  
        $MERQURY/merqury.sh $meryl $unpolished unpolished;

        # run polished
        $MERQURY/merqury.sh $meryl $polished pilon;
        """


rule meryl:
    input:
        fastq_files = expand( "../data/{{source_data}}__{direction}.fastq.gz", direction=directions ),
        k_file = "../assembly/best_k.txt"
    output:
        expand( "../data/{{source_data}}__{direction}.fastq.gz.meryl", direction=directions ) 
    shell:
        """
        module load UHTS/Assembler/canu/2.1.1;
        k=$(cat {input.k_file});
        for i in {input.fastq_files}; do
            meryl k=$k count $i output $i.meryl;
        done;
        """

rule meryl_merge:
    input:
        expand( "../data/{{source_data}}__{direction}.fastq.gz.meryl", direction=directions ),
    output:
       directory( "../polishing/all_kmers__{source_data}.meryl" )
    shell:
        """
        module load UHTS/Assembler/canu/2.1.1;
        meryl union-sum output {output} {input};
        """

rule get_best_k:
    conda:
        "../envs/merqury.yaml"
    input:
        "../assembly/genomesize.txt"
    output:
        "../assembly/best_k.txt"
    params:
        tolerable_collision_rate = 0.1,
        backup_k = 21
    shell:
        """
        # the best_k.sh just echoes what input it gets but tries some division by zero and fails, 
        # so we use just 21...
        # genome_size=$(tail -n1 {input} | head -n1);
        # sh $MERQURY/best_k.sh $genome_size tolerable_collision_rate={params.tolerable_collision_rate} > {output};
        echo {params.backup_k} > {output};
        """

rule copy_pilon_to_assembly:
    input:
        "../polishing/{source_data}/{assembler}/pilon_{source_short}.fasta"
    output:
        "../assembly/{source_data}/{assembler}_pilon_{source_short}/assembly.fasta"
    shell:
        """
        cp {input} {output};
        """

ruleorder: copy_pilon_to_assembly > get_fasta_for_assembly

rule busco_single:
    input:
        "../assembly/{source_data}/{assembler}/assembly.fasta",
    output:
        directory( "../tmp_summary/busco/{source_data}/{assembler}" )
    params:
        mode = "genome",
        lineage = "brassicales_odb10",
    log:
        "../busco_{source_data}_{assembler}.log"

    shell:
        """
        sbatch ./scripts/busco.slurm {input} {output} busco {params.mode} {params.lineage};
        """

    # this below actually seems very promising but somehow it keeps crashing...
    # shell:
    #     """
    #     infile=$( readlink -f {input} );
    #     outdir=$(readlink -f "../tmp_summary/busco/{wildcards.source_data}/" );

    #     cd {base};
    #     module load UHTS/Analysis/busco/4.1.4;

    #     if [[ ! -d "augustus_config" ]]; then
    #         echo "Copying augustus_config to parent directory";
    #         cp -r /software/SequenceAnalysis/GenePrediction/augustus/3.3.3.1/config augustus_config
    #     fi
        
    #     # make a copy of the augustus_config
    #     addon=$( ls | grep -c augustus_config );
    #     augustus_dir="augustus_config_$addon";
    #     cp -r augustus_config $augustus_dir;
    #     chmod -R 777 $augustus_dir;

    #     export AUGUSTUS_CONFIG_PATH=$( readlink -f $augustus_dir );

    #     cd $outdir;
    #     mkdir -p {wildcards.assembler};   
    #     cd {wildcards.assembler};

    #     # run busco
    #     busco -i $infile -o {wildcards.assembler} -l {params.lineage} -m {params.mode} -c 10 --out_path $outdir;

    #     # remove the augustus_config copy
    #     rm -r $AUGUSTUS_CONFIG_PATH;
    #     """

rule busco:
    input:
        unpolished = expand( "../tmp_summary/busco/{{source_data}}/{assembler}", assembler = ["canu", "flye"] ), 
        polished = expand( "../tmp_summary/busco/{{source_data}}/{assembler}_pilon_Illumina", assembler = ["canu", "flye"] ) 
    output:
        "../summary/busco/{source_data}"
    shell:
        """
        mkdir -p {output};
        for i in {input.unpolished}; do
            cp -r $i {output}/$(basename $i);
        done;

        for i in {input.polished}; do
            cp -r $i {output}/$(basename $i);
        done;
        """

rule quast:
    input:
        unpolished = expand( "../assembly/{{source_data}}/{assembler}/assembly.fasta", assembler = ["canu", "flye"] ), 
        polished = expand( "../assembly/{{source_data}}/{assembler}_pilon_Illumina/assembly.fasta", assembler = ["canu", "flye"] ) 
    output:
        directory( "../summary/quast/{source_data}" )
    params:
        genome = f"{base}/reference/*.fas",
        gff = f"{base}/reference/*.gff"
    threads: 15
    run:
        import os, glob
        # os.makedirs( str(output), exist_ok=True )

        genome = glob.glob( params.genome )[0]
        gff = glob.glob( params.gff )[0]
        labels = ",".join( [ os.path.basename( os.path.dirname(i) ) for i in input.unpolished + input.polished ] ) 
        cmd = f"""
        module load UHTS/Quality_control/quast/4.6.0;
        quast.py --no-sv --eukaryote -o {output} -t {threads} -R {genome} -G {gff} -l "{labels}" {input.unpolished} {input.polished};
        """
        shell( cmd )

rule mummer:
    input:
        query = "../assembly/{source_data}/{assembler}/assembly.fasta",
        reference = glob.glob( f"../reference/*.fas" )[0]
    output:
        "../tmp_summary/mummer/{source_data}/{assembler}_mummerplot.png"
    params:
        breaklen = 1000,
        mincluster = 1000,

    threads: 
        10
    shell:
        """
        
        input=$( readlink -f {input.query} );
        reference=$( readlink -f {input.reference} );

        directory="../tmp_summary/mummer/{wildcards.source_data}/{wildcards.assembler}";
        mkdir -p $directory ;
        cd $directory;

        module add UHTS/Analysis/mummer/4.0.0beta1;
        PATH=/software/bin:$PATH;

        nucmer -t {threads} --breaklen {params.breaklen} --mincluster {params.mincluster} --prefix {wildcards.assembler}_mummer --maxmatch $reference $input
        
        mummerplot --layout -t png --large --fat -R $reference -Q $input --filter --layout --prefix {wildcards.assembler}_mummerplot {wildcards.assembler}_mummer.delta;
        mv {wildcards.assembler}_mummerplot.png .. ;
        """ 