#!/bin/bash
# This script is designed to perform the basic setup for the project 


parent="/data/users/noahkleinschmidt/genome_assembly"

cd $parent

# initialize records for the project
if [ ! -d ".registry" ]; then
    records init -c "The genome assembly course project"
fi

# initialize a git repo for the project
if [ ! -d ".git" ]; then
    git init
    echo "data" >> .gitignore
    echo ".registry" >> .gitignore
fi



# get the raw data
mkdir data

srcdir="/data/courses/assembly-annotation-course/raw_data/Cvi-0/participant_3/"

# the shitty ln -s does not work... 

cd data
touch sources.txt
for i in $(ls $srcdir); do 
    
    echo "copying files from: $i"

    # copy the data file
    echo "$i" >> $parent/data/sources.txt

    cd $srcdir/$i
    for j in $(ls); do
        cp $j "$parent/data/${i}__${j}"
    done

    echo "Done copying, now renaming to R1/R2"

    # rename to R1 R2 to make the datafile nomencalture consistent

    cd $parent/data
    for j in $( ls ${i}__* ); do

        direction=$( echo $j | grep -oP "(1|2).fastq.gz" | cut -c 1  )

        if [ $direction == "1" ]; then
            suffix="R1"
        else
            suffix="R2"
        fi

        # rename the file and add records to remember the original file names
        mv "$j" "${i}__${suffix}.fastq.gz"
        original=$( echo $j | grep -oP "(?<=__)(.+)(?=.fastq.gz)" )
        records comment "${i}__${suffix}.fastq.gz" -c "File origin: $i__$suffix -> $i/$original" -f raw_data data
        
    done

    echo "Done renaming"

done