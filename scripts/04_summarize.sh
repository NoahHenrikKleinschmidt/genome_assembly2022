#!/bin/bash

# This script performs assembly quality summarization

parent="/data/users/$USER/genome_assembly" # Path to the parent directory
pipelines="$parent/pipelines" # Path to the pipelines directory

cd "$pipelines"

data_source="pacbio"
assemblers="canu flye"

# call on busco to summarize the assemblies
snakemake ../summary/busco/$data_source/ --cluster="sbatch --cpus-per-task=10 --job-name=buscos --partition=pcourseassembly --mem=50G --time=1-00:00:00" -j 5

# call on quast to summarize the assemblies
snakemake ../summary/quast/$data_source/ --cluster="sbatch --cpus-per-task=10 --job-name=quast --partition=pcourseassembly --mem=50G --time=1-00:00:00" -j 5

# call on merqury to summarize the assemblies
for assembler in $assemblers; do
    snakemake ../summary/merqury/$data_source/$assembler/Illumina --cluster="sbatch --cpus-per-task=10 --job-name=merqury --partition=pcourseassembly --mem=50G --time=1-00:00:00" -j 5
done

# now collect all results into nice tables
python3 .04_collect_busco.py $data_source
python3 .04_collect_quast.py $data_source
python3 .04_collect_merqury.py $data_source