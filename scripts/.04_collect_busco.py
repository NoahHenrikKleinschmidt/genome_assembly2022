"""
This script collects busco results from multiple raw short_summary text files into a single table
"""
import os, glob
import pandas as pd
import argparse

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns

parent=f"/data/users/{os.environ['USER']}/genome_assembly" # Path to the parent directory
pipelines=f"{parent}/pipelines" # Path to the pipelines directory
summaries=f"{parent}/summary/busco/{{data_source}}"

def cli():
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('input', help='Path to the input directory')
    parser.add_argument('-o', '--output', help='Path to the output file, by default a "summary.tsv" file within the input directory', nargs="?", default=None)
    args = parser.parse_args()
    return args

def main():

    args = cli()

    os.chdir(summaries.format( data_source = args.input ))
    assemblies = os.listdir()
    df = collect_df(args, assemblies)
    plot_summary( df, f"{summaries.format(data_source=args.input)}/summary.pdf" )
    


def collect_df(args, assemblies):

    dfs = []

    for assembly in assemblies:
        if not os.path.isdir(assembly):
            continue

        busco = glob.glob( f"{assembly}/busco/short*" )[0]

        # mine out the busco results from the short summary textfile
        with open( busco, 'r') as f:
            content = f.readlines()[-6:]
            content = [x.strip() for x in content]
            content = [x.split("\t") for x in content]
            content = list( zip( *content ) )
            content = { assembly : content[0], "label" : content[1] }

        df = pd.DataFrame( content ).transpose()
        df.columns = df.iloc[1]
        df = df.drop( df.index[1] )
        dfs.append( df )

    df = pd.concat( dfs )
    
    if args.output is None:
        args.output = f"{summaries.format(data_source=args.input)}/summary.tsv"
    df.to_csv( args.output, sep="\t" )

    return df 

def plot_summary( df, filename ):

    df1 = df.melt( ignore_index = False )
    df1 = df1.reset_index()
    df1["value"] = df1["value"].astype(float)

  
    sns.set_palette( "mako_r" )
    sns.barplot( x = "value", y = "label", hue = "index", data = df1 )
    ax = plt.gca()
    ax.set( xscale = "log", xlabel = "count", ylabel = "" )
    ax.legend( bbox_to_anchor = (1,1), frameon = False )
    sns.despine()

    plt.savefig( filename, bbox_inches = "tight" )

if __name__ == "__main__":
    main()