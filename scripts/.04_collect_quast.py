"""
This script collects quast results from multiple raw report text files into multiple tables
"""
import os, glob
import pandas as pd
import argparse

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns

parent=f"/data/users/{os.environ['USER']}/genome_assembly" # Path to the parent directory
pipelines=f"{parent}/pipelines" # Path to the pipelines directory
summaries=f"{parent}/summary/quast/{{data_source}}"

def cli():
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('input', help='Path to the input directory')
    parser.add_argument('-o', '--output', help='Path to the output file, by default a "summary.tsv" file within the input directory', nargs="?", default=None)
    args = parser.parse_args()
    return args

def main():

    args = cli()

    print( summaries.format( data_source = args.input ))
    os.chdir(summaries.format( data_source = args.input ))
    current_outputs = glob.glob("summary*")
    for output in current_outputs:
        os.remove(output)
    assemblies = os.listdir()
    contigs, n50, genome_fraction, misassemblies, errors = collect_dfs(args, assemblies)
    plot_summary( contigs, n50, genome_fraction, misassemblies, errors, f"{summaries.format(data_source=args.input)}/summary.{{plot}}.pdf" )
    


def collect_dfs(args, assemblies):

    contigs = []
    n50 = []
    genome_fraction = []
    misassemblies = []
    mismatches = []
    indels = []

    for assembly in assemblies:

        if not os.path.isdir(assembly):
            continue

        quast = glob.glob( f"{assembly}/report.tsv" )[0]

        # mine out the quast results from the short summary textfile
        with open( quast, 'r') as f:
            content = f.readlines()
        
        # get contigs
        contigs.append( mine_contigs(assembly, content) )
        
        # get N50
        n50.append( mine_n50(content) )

        # get genome fraction
        genome_fraction.append( mine_genome_fraction(content) )

        # get misassemblies
        misassemblies.append( mine_misassemblies(content) )

        # get mismatches and indels
        m, i = mine_errors( content )
        mismatches.append( m )
        indels.append( i )


    contigs = pd.concat(contigs)
    # contigs = contigs.reset_index()
    # contigs = contigs.rename( columns = { "index" : "label" } )
    n50 = pd.DataFrame( { "label" : assemblies, "n50" : n50 } )
    genome_fraction = pd.DataFrame( { "label" : assemblies, "genome_fraction" : genome_fraction } )
    misassemblies = pd.DataFrame( { "label" : assemblies, "total" : [ i[0] for i in misassemblies ], "local" : [ i[1] for i in misassemblies ] } )
    errors = pd.DataFrame( { "label" : assemblies, "mismatches" : mismatches, "indels" : indels } )

    if not args.output:
        args.output = f"{summaries.format(data_source=args.input)}/{{filename}}.tsv"
    
    contigs.to_csv( args.output.format(filename="summary.contigs"), sep="\t", index=False )
    n50.to_csv( args.output.format(filename="summary.n50"), sep="\t", index=False )
    genome_fraction.to_csv( args.output.format(filename="summary.genome_fraction"), sep="\t", index=False )
    misassemblies.to_csv( args.output.format(filename="summary.misassemblies"), sep="\t", index=False )
    errors.to_csv( args.output.format(filename="summary.errors"), sep="\t", index=False )

    return contigs, n50, genome_fraction, misassemblies, errors

def mine_errors( content ):

    mismatches = [ i for i in content if "mismatches" in i ][0]
    mismatches = mismatches.split("\t")[1]
    mismatches = float( mismatches.strip() )

    indels = [ i for i in content if "indels" in i ][0]
    indels = indels.split("\t")[1]
    indels = float( indels.strip() )

    return mismatches, indels

def mine_misassemblies( content ):
    misassemblies = [ i for i in content if "misassemblies" in i ]
    misassemblies = [ i.replace("# ", "") for i in misassemblies ]
    misassemblies = [ i.split("\t") for i in misassemblies ]
    misassemblies = list( zip( *misassemblies ) )
    misassemblies = [ int( i ) for i in misassemblies[1] ]

    return misassemblies

def mine_genome_fraction( content ):

    genome_fraction = [ i for i in content if "Genome fraction" in i ][0]
    genome_fraction = genome_fraction.split("\t")[1]
    genome_fraction = float( genome_fraction )
    return genome_fraction

def mine_n50( content ):
    
    n50 = [ i for i in content if "N50" in i ][0]
    n50 = n50.split("\t")[1]
    n50 = int( n50.strip() )
    return n50

def mine_contigs(assembly, content):
    contigs = content[1:7]
    contigs = [x.strip().replace("# ", "") for x in contigs]
    contigs = [x.split("\t") for x in contigs]
    contigs = list( zip( *contigs ) )
    contigs = { "label" : contigs[0], assembly : contigs[1] }

    contigs = pd.DataFrame( contigs ).transpose()
    contigs.columns = contigs.iloc[0]
    contigs = contigs.drop( contigs.index[0] )
    return contigs

    

def plot_summary( contigs, n50, genome_fraction, misassemblies, errors, filename ):

    sns.set_palette( "mako_r" )
    sns.set_style( "ticks" )

    # contigs.set_index("label", inplace=True)
    contigs = contigs.melt( ignore_index=False ).reset_index()
    contigs["value"] = contigs["value"].astype(int)
    
    contigs = sns.barplot( data = contigs, x = "index", y = "value", hue = "label" )
    contigs.legend( bbox_to_anchor=(1.05, 1), loc=2, frameon = False )
    contigs.set( ylabel = "Number of contigs" )
    contigs.set( xlabel = "" )
    sns.despine()
    contigs.figure.savefig( filename.format( plot = "contigs" ), bbox_inches = "tight" )
    plt.close()


    fig, ax = plt.subplots( figsize = (6, 2), dpi = 200 )
    sns.set_palette( "mako_r" )
    ax = errors.plot.barh( stacked = True, ax = ax )
    ax.set( yticklabels = errors.label, xlabel = "Aberrations per 100kb" )
    ax.legend( bbox_to_anchor = (1,1), frameon=False)
    sns.despine()
    fig.savefig( filename.format( plot = "errors" ), bbox_inches = "tight" )


    fig, ax = plt.subplots( figsize = (5,2), dpi = 200 )
    genome_fraction.plot.barh( ax = ax, legend = False )
    ax.set( xlim = (80,90), yticklabels = genome_fraction.label, xlabel = "%", title = "Assembled Genome Fraction" )
    sns.despine()
    fig.savefig( filename.format( plot = "genome_fraction" ), bbox_inches = "tight" )


    fig, ax = plt.subplots( figsize = (5,2), dpi = 200 )
    misassemblies.plot.barh( ax = ax, width = 0.8 )
    ax.set( yticklabels = misassemblies.label, xlabel = "Misassemblies" )
    ax.legend( bbox_to_anchor = (1,1), frameon = False )
    sns.despine()
    fig.savefig( filename.format( plot = "misassemblies" ), bbox_inches = "tight" )


    fig, ax = plt.subplots( figsize = (5,2), dpi = 200 )
    n50.plot.barh( ax = ax, width = 0.8, legend = False )
    ax.set( yticklabels = n50.label, xlabel = "bp", title = "N50" )
    sns.despine()
    fig.savefig( filename.format( plot = "n50" ), bbox_inches = "tight" )


if __name__ == "__main__":
    main()