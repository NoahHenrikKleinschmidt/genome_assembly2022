#!/bin/bash

# This script runs the main assemblies

parent="/data/users/$USER/genome_assembly" # Path to the parent directory
pipelines="$parent/pipelines" # Path to the pipelines directory

cd "$pipelines"

# first make the genomesize files so that canu and flye know what to work with
# since genome size estimation is best for the Illumina reads, we will use those
snakemake -j 1 --cores 1 ../assembly/Illumina/genomesize.txt

cd "$scripts" # Change to the pipelines directory

# call on canu through the pipeline
sbatch ./.02_call_canu.slurm $1

# call on flye through the pipeline
sbatch ./.02_call_flye.slurm $1

# call on trinity through the pipeline
sbatch ./.02_call_trinity.slurm $1

