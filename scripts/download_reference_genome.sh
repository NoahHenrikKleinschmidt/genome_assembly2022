#!/bin/bash

# This script downloads reference genomes for Arabidopsis thaliana 


parent="/data/users/$USER/genome_assembly" # Path to the parent directory
pipelines="$parent/pipelines" # Path to the pipelines directory
ref_dir="$parent/reference"

cd "$parent" # Change to the pipelines directory

# Create a directory for the reference genomes
if [ ! -d "$ref_dir" ]; then
    mkdir -p "$ref_dir"
fi

# Download the reference genome
fasta_link="https://www.arabidopsis.org/download_files/Genes/TAIR10_genome_release/TAIR10_chromosome_files/TAIR10_chr_all.fas.gz"
gff_link="https://www.arabidopsis.org/download_files/Genes/TAIR10_genome_release/TAIR10_gff3/TAIR10_GFF3_genes.gff"

# Download the reference genome
if [ ! -f "$ref_dir/TAIR10_chr_all.fas" ]; then
    wget -O "$ref_dir/TAIR10_chr_all.fas.gz" "$fasta_link"
    gunzip "$ref_dir/TAIR10_chr_all.fas.gz"
fi

# Download the reference annotation
if [ ! -f "$ref_dir/TAIR10_GFF3_genes.gff" ]; then
    wget -O "$ref_dir/TAIR10_GFF3_genes.gff" "$gff_link"
fi
