"""
This script collects merqury results from multiple raw spectra-cn.hist text files into a single table
"""

import os, glob
import pandas as pd
import argparse

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns

parent=f"/data/users/{os.environ['USER']}/genome_assembly" # Path to the parent directory
pipelines=f"{parent}/pipelines" # Path to the pipelines directory
summaries=f"{parent}/summary/merqury/{{data_source}}"

def cli():
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('input', help='Path to the input directory')
    parser.add_argument('-o', '--output', help='Path to the output file, by default a "summary.tsv" file within the input directory', nargs="?", default=None)
    args = parser.parse_args()
    return args

def main():

    args = cli()

    os.chdir(summaries.format( data_source = args.input ))
    assemblies = os.listdir()
    df = collect_df(args, assemblies)
    plot_summary( df, f"{summaries.format(data_source=args.input)}/summary.pdf" )
    


def collect_df(args, assemblies):

    dfs = []
    label = lambda x: "" if x.split("/")[-1].split(".")[0] == "unpolished" else "_" + x.split("/")[-1].split(".")[0]

    for assembly in assemblies:
        if not os.path.isdir(assembly):
            continue

        hists = glob.glob( f"{assembly}/Illumina/*spectra-cn.hist" )
        for hist in hists:
            df = pd.read_csv( hist, sep = "\t", header = 0 )
            df.columns = [ i.lower() for i in df.columns ]
            df["label"] = f"{assembly}{label(hist)}"
            dfs.append(df)

    df = pd.concat( dfs, ignore_index = True )
    
    if args.output is None:
        args.output = f"{summaries.format(data_source=args.input)}/summary.tsv"
    df.to_csv( args.output, sep="\t", index = False )

    return df 

def plot_summary( df, filename ):

    df = df.query( "kmer_multiplicity <= 100" )
    per_label = df.groupby( "label" )
    fig, axs = plt.subplots( ncols = len(df.label.unique()), figsize = (10,3) )
    
    sns.set_palette( "mako_r" )

    idx = 0
    for label, df in per_label:
        ax = axs.flat[idx]

        sns.lineplot( data = df, x = "kmer_multiplicity", y = "count", hue = "copies", ax = ax, legend = idx == len(axs)-1 )
        ymax = df.query( "copies == '1'" )["count"].max() * 1.1
        ax.set( ylim = (0,ymax), title = label )
        idx += 1

    ax.legend( bbox_to_anchor = (1,1), frameon = False )
    sns.despine()
    plt.tight_layout()

    fig.savefig( filename, bbox_inches = "tight" )

if __name__ == "__main__":
    main()