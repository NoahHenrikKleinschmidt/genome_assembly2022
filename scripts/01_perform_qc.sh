#!/bin/bash

# This script performs quality control on the raw reads using FastQC and summarizes with MultiQC

parent="/data/users/$USER/genome_assembly" # Path to the parent directory
pipelines="$parent/pipelines" # Path to the pipelines directory

cd "$pipelines" # Change to the pipelines directory

# Create a directory for the quality control results
mkdir -p "$parent/qc"

echo "Starting quality control"
echo "Running FastQC"

# call the pipelines to perform quality control
snakemake  -j ../qc/multiqc_report.html \
            --use-conda \
            --cores 1 \
            --conda-frontend mamba \
            --cluster "sbatch --time=01:00:00 --cpus-per-task=5 --mem-per-cpu=50G --partition=pcourseassembly" \
            --jobs 10 #\
            #-p -n
   
echo "Done running FastQC"
echo "Running JellyFish"

# this will generate an array of sources e.g. Illumina pacbio nanopore ... etc
sources=$( cat $parent/data/sources.txt |  tr "\n" " " )

for source in $sources; do
    snakemake -j ../qc/kmers/${source} \
                --cores 1 \
                --cluster "sbatch --time=01:00:00 --cpus-per-task=10 --mem-per-cpu=20G --partition=pcourseassembly" \
                --jobs 1 $1
done

echo "Done with quality control"