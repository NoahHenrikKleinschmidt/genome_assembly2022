#!/bin/bash

# This script performs assembly polishing with Illumina reads

parent="/data/users/$USER/genome_assembly" # Path to the parent directory
pipelines="$parent/pipelines" # Path to the pipelines directory

cd "$scripts"

# call the pipeline to map Illumina to pacbio canu/flye
# and use pilon afterward as well
data_sources="pacbio"
for data_source in $data_sources; do
    sources="canu flye"
    short_read_source="Illumina"
    for source in $sources; do
        sbatch .03_align_short_reads.slurm $data_source/$source $short_read_source
    done
done