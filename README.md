# genome_assembly2022

This is the course-project repository for the genome assembly course [473637-HS2022-0: Genome and Transcriptome Assembly](http://ilias.unibe.ch/ksl/473637-HS2022-0). All important files for the project directory and analysis pipeline will be stored in this repository. No data, neither raw nor intermediary, will be stored, however. 


## Data availablility
This project uses data from [Jiao and Schneeberger (2020)]( http://dx.doi.org/10.1038/s41467-020-14779-y ). 
Specifically, this project works with the `Cvi-0/participant_3` dataset. 

## Setup
A shell script is available in the `setup` directory to move the corresponding data files (from a local directory, not a remote storage!) to the appropriate location within the project directory for the data pipelines to work. 

